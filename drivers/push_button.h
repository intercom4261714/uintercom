/**
 * \file power_button.h
 * \author Vladimir Poliakov
 * \version 1.0
 */
#pragma once

#include "event_trigger.h"

#include "Timeout.h"

#include <chrono>
#include <memory>

namespace uintercom {
/**
 * \brief Event triggering button with a debouncer
 * Emits an event every time it is pushed/released
 * \note This button uses a software timer for dejittering (100ms default)
 */
class PushButton : public EventTrigger
{
public:
    typedef events::Event<void (bool)> Event;
    typedef std::unique_ptr<Event> EventSPtr;

    /**
     * Class ctr
     * \param input Button pin
     */
    PushButton(PinName input);

    /**
     * Class ctr
     * \param input Button pin
     * \param event Triggered event
     */
    PushButton(PinName input, EventSPtr &event);

    /** 
     * Pressed property setter 
     * Triggers an event every time pressed is changed
     */
    void setPressed(bool b);

    /** Pressed property getter */
    inline bool pressed() { return m_pressed; }

private:
    virtual void connectEvent() override {}

    void init();

    bool m_pressed {false};
    mbed::Timeout m_dejitter;
    std::chrono::milliseconds m_dejitterDelay {100};
};
}