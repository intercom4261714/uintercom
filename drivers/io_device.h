/**
 * \file io_device.h
 * \author Vladimir Poliakov
 */
#pragma once

#include <mbed_retarget.h>
#include <stdint.h>

namespace uintercom {
/**
 * \brief Класс интерфеса устройств ввода/вывода
 *
 * Предназначен для определения интерфейса
 * записи/считывания данных из устройств ввода/вывода
 * (UART и пр.)
 */
class IODevice {
public:
    /** Открыть устройство */
	virtual void open() = 0;

    /** Закрыть устройство */
	virtual void close() = 0;

    /** Считать данные
     * \param data Буфер для считанных данных
     * \param length Количество байт для чтения
     * \return количество считанных байт, отрицательное значение при ошибке
     */
	virtual ssize_t read(uint8_t *data, size_t length) = 0;

    /** Записать данные
     * \param data Буфер данных для записи
     * \param length Количество байт для записи
     * \return количество записанных байт, отрицательное значение при ошибке
     */
	virtual ssize_t write(const uint8_t *data, size_t length) = 0;

    /** Очистить буфер приема */
    virtual void flush() = 0;

protected:
	~IODevice() {}
};

}