#include "push_button.h"

using Event = events::Event<void (void)>;

namespace uintercom {

PushButton::PushButton(PinName input) :
    EventTrigger(input)
{
    init();
}

PushButton::PushButton(PinName input, EventSPtr &event) :
    EventTrigger(input, event)
{
   init();
}

void PushButton::setPressed(bool value)
{
    if (m_pressed == value) return;
    m_pressed = value;
    if (m_event) m_event->post(m_pressed);
}

void PushButton::init()
{
    m_input.mode(PullUp);
    m_input.fall([&](){
        m_dejitter.attach([&]() { if (!m_input) setPressed(true);}, m_dejitterDelay);
        });
    m_input.rise([&](){
        setPressed(false);
        m_dejitter.detach();
        });
}

}
