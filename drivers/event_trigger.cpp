#include "event_trigger.h"

namespace uintercom {

EventTrigger::EventTrigger(PinName pin) :
    m_input(pin)
{
    m_input.mode(PullUp);
}

EventTrigger::EventTrigger(PinName pin, EventTrigger::EventSPtr &event) :
    m_input(pin)
{
    m_input.mode(PullUp);
    setEvent(event);
}

void EventTrigger::setEvent(EventTrigger::EventSPtr &ev)
{
   m_event = std::move(ev);
   connectEvent();
}

void EventTrigger::connectEvent()
{
    m_input.rise([&](){m_event->post(true);});
    m_input.fall([&](){m_event->post(false);});
    m_event->post(m_input.read());
}

}