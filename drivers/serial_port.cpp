#include "serial_port.h"

namespace uintercom {

SerialPort::SerialPort(PinName tx, PinName rx, int baud) :
    m_driver(tx, rx, baud)
{
    m_driver.set_blocking(false);
}

void SerialPort::open()
{
	// Not used in mbed
}

void SerialPort::close()
{
	// Not used in mbed
}

ssize_t SerialPort::read(uint8_t *data, size_t length)
{
	return m_driver.read(data, length);
}

ssize_t SerialPort::write(const uint8_t *data, size_t length)
{
	return m_driver.write(data, length);
}

void SerialPort::flush()
{
    m_driver.sync();
}

}