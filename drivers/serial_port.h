/**
 * \file serial_port.h
 * \author Vladimir Poliakov
 * \version 1.0
 */
#pragma once

#include "drivers/io_device.h"

#include <BufferedSerial.h>

namespace uintercom {
/**
 * Serial communication port (UART)
 * \note This driver operates in the non-blocking mode, 
 * thus it can return incomplete packages, should the buffer be empty
 * \sa IODevice
 */
class SerialPort : public IODevice
{
public:
    typedef mbed::BufferedSerial Lld;

    /**
     * Class ctor
     * \param tx Transmitting pin
     * \param rx Receiving pin
     * \param baud Baud rate
     */
	SerialPort(PinName tx, PinName rx, int baud = MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE);
	~SerialPort() {}

    /** Open the device */
	virtual void open() override;

    /** Close the device */
	virtual void close() override;

    /**
     * Read data into the rx buffer
     * \param data Rx buffer
     * \param length Number of bytes to read
     * \return Number of bytes read
     */
	virtual ssize_t read(uint8_t *data, size_t length) override;

    /**
     * Write data from tx buffer
     * \param data Tx buffer
     * \param length Number of bytes to be written
     * \return Number of bytes written
     */
	virtual ssize_t write(const uint8_t *data, size_t length) override;

    /** Clear device input */
    virtual void flush() override;

    /** Set baud rate */
    void setBaudRate(int speed);

private:
    Lld m_driver;
};
}