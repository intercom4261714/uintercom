/**
 * \file detector
 * \author Vladimir Poliakov
 * \version 1.0
 */
#pragma once

#include <memory>

#include "Event.h"
#include "InterruptIn.h"

namespace uintercom {
/**
 * \brief Event-triggering external device
 * Emits an event every time the external interrup occurs
 */
class EventTrigger
{
public:
    typedef events::Event<void (bool)> Event;
    typedef std::unique_ptr<Event>  EventSPtr;

    /**
     * Class ctor
     * \param pin Trigger input pin
     */
    EventTrigger(PinName pin);

    /**
     * Class ctor
     * \param pin Trigger input pin
     * \param event Triggered event
     */
    EventTrigger(PinName pin, EventSPtr &event);

    /** Triggered event setter */
    void setEvent(EventSPtr &ev);

protected:
    virtual void connectEvent();

    EventSPtr m_event;  
    mbed::InterruptIn m_input;
};
}