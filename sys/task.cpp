#include "task.h"
#include "Kernel.h"
#include "ThisThread.h"

using namespace std::chrono_literals;

namespace uintercom {

Task::Task(const std::chrono::milliseconds &period, osPriority_t priority) :
    m_period(period),
    m_priority(priority)
{
}

void Task::start()
{
    if (m_running || !m_enabled)
        return; 
        
    m_running = true;

    // create a new thread that will execute
    // the main task functionality
    m_thread = std::make_unique<rtos::Thread>(m_priority, m_taskSize);
    m_thread->start([&]() {
        init();
        while (m_running) {
            auto wakeup = rtos::Kernel::Clock::now();
            update();
            rtos::ThisThread::sleep_until(wakeup + m_period);
        }
        cleanup();
    });
}

void Task::finish()
{
    m_running = false;
    m_thread->join();
}

void Task::disable()
{
    if (m_running) 
        finish(); 
    
    m_enabled = false;
}

}