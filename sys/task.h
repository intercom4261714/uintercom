/**
 * \file task.h
 * \author Vladimir Poliakov
 */
#pragma once

#include <Thread.h>
#include <chrono>
#include <memory>


namespace uintercom {
/**
 * \brief Abstract periodic task class 
 *
 * Defines interfaces and basic functionality
 * of a peridic task
 */
class Task
{
public:
    /**
     * \brief Class ctor
     * \param period Repetition period
     * \param priority Task priority
     */
    Task(const std::chrono::milliseconds &period = std::chrono::milliseconds(50),
         osPriority_t priority = osPriorityNormal);
    virtual ~Task() = default;

    /** Start the task */
    void start();

    /** Stop the task */
    void finish();

    /**
     * \brief Enable task execution
     *
     * If enabled, the task can be strated and stopped
     */
    inline void enable() { m_enabled = true; }

    /**
     * \brief Disable task execution
     *
     * When disabled, the task cannot be start.
     * If the task is already started , it will be stopped
     */
     void disable();

    /**
     * Enable/disable the task 
     * \sa enable
     * \sa disable
     */
     inline void setEnabled(bool value) { value ? enable() : disable(); }

    /**
     * Enabled status getter
     * \sa setEnabled
     * \return true if the task is enabled, false overwise
     */
     inline bool isEnabled() { return m_enabled; }

    /**
     * Running status getter
     * \return true if the task is running, false overwise
     */
    inline bool isRunning() { return m_running; }

protected:
    /**
     * \brief Init function
     *
     * Init is executed with each start of the task
     * before the main task loop
     */
    virtual void init() = 0;

    /**
     * \brief Cleanup function
     *
     * Cleanup is executed when the task is stopped
     * after the main task loop
     */
    virtual void cleanup() = 0;

    /**
     * \brief Main loop update function
     *
     * Update is executed with each iteration of the
     * main task loop
     */
    virtual void update() = 0;

private:
    bool m_running {false};
    bool m_enabled {true};
    std::unique_ptr<rtos::Thread> m_thread;
    std::chrono::milliseconds m_period;
    osPriority_t m_priority;
    size_t m_taskSize {1024};
};
}