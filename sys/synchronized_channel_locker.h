/**
 * \file synchronized_channel_locker.h
 * \author Vladimir Poliakov
 */
#pragma once

#include "channel.h"
#include <vector>
#include <initializer_list>


namespace uintercom {

/**
 * \brief The synchronized channel locker class
 *
 * Implements simulatinious lock of multiple channels.
 * Locks the channels sequentially; if any of the locking channels
 * is locked by another thread, releases all acquired channels
 * and yields till the next iteration. Loops until all channels are locked
 */
class SynchronizedChannelLocker
{
public:
  SynchronizedChannelLocker(std::initializer_list<BaseChannel *> channels);
  void lock();
  void unlock();

  inline void add(BaseChannel *channel) { m_channels.push_back(channel); }

private:
  std::vector<BaseChannel *> m_channels;
};

}