#include "synchronized_channel_locker.h"
#include <ThisThread.h>

using namespace std::chrono_literals;

namespace uintercom {

SynchronizedChannelLocker::SynchronizedChannelLocker(std::initializer_list<BaseChannel *> channels) :
m_channels(channels)
{
}

void SynchronizedChannelLocker::lock()
{
  bool available = false;
  while(!available) {
    // try to lock acquire all channels at once
    available = true;
    for (auto &ch : m_channels) {
      if (!ch->trylock()) {
        available = false;
      }
    }

    // if locking failed, release all acuired channels and yield
    if (!available) {
      unlock();
      rtos::ThisThread::sleep_for(1ms);
    }
  }
}

void SynchronizedChannelLocker::unlock()
{
  for (auto &ch : m_channels) {
      if (ch->isOwner()) {
        ch->unlock();
      }
  }
}

}