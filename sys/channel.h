/**
 * \file channel.h
 *
 * The intertask communication class
 *
 * \author Vladimir Poliakov
 */
#pragma once

#include "Mutex.h"
#include "ThisThread.h"

#include <memory>

namespace uintercom {

/**
 * \brief Channel communication base class
 *
 * Basic functionality to lock/unlock the mutex
 * gurading the data.
 * \sa SynchronizedChannelLocker
 */
class BaseChannel {
public:
  virtual ~BaseChannel() = default;

  inline void lock() { m_mtx.lock(); }
  inline void unlock() { m_mtx.unlock(); }
  inline bool trylock() { return m_mtx.trylock(); }

  inline bool isOwner() { return m_mtx.get_owner() == rtos::ThisThread::get_id(); }

private:
  rtos::Mutex m_mtx;
};

/**
 * \brief Channel communication primitive
 *
 * Provides exclusive acces to encapsulated data,
 * protecting it from simultanious reading/writing
 * when working with several threads.
 *
 * To access the data, the user should call
 * acquire(). Once done, call release()
 * to release the lock on the data.
 */
template <typename T>
class Channel : public BaseChannel {
public:
    typedef std::shared_ptr<Channel<T>> SPtr;
    virtual ~Channel() override = default;

    T & acquire() { if (!isOwner()) lock(); return m_buffer; }
    void release() { unlock(); }

private:
    T m_buffer;
};

}