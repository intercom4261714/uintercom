/**
 * \file intercom.h
 * \author Vladimir Poliakov
 */
#pragma once

#include <memory>

#include "Event.h"

#include "io_device.h"
#include "sys/channel.h"

namespace uintercom {

/**
 * \brief Internode communication class
 *
 * Buffered communication with peer nodes (co-processors).
 *
 * \tparam <InpMsg> Input message struct (w/o the STX and checksum bytes)
 * \tparam <OupMsg> Output message struct (w/o the STX and checksum bytes)
 * \tparam <buffSize> Package size (w/o the STX and checksum bytes)
 *
 * \note The STX and checksum bytes are added automatically,
 * which is why the actual size of the package is @p buffSize + 2.
 *
 * \sa IODevice
 * \sa Channel
 */
template <typename InpMsg, typename OupMsg, size_t bufSize> class Intercom {
public:
  typedef events::Event<void(bool)> Event;
  typedef std::unique_ptr<Event> EventSPtr;
  /**
   * Class ctor
   * \param ioDevice Pointer to the IO Device
   * \param lostConnectionEvent Event that is triggered when lost connection is detected
   */
  Intercom(IODevice *ioDevice);

  /** Initialization routine*/
  void init();

  /** De-initialization routine*/
  void deinit();

  /** Re-initialization */
  inline void reinit() {
    deinit();
    init();
  }

  /** One-time transmission/retrieval */
  inline void update() {
    transmit();
    receive();
  }

  /** Transmit the current package */
  void transmit();

  /** Receive a package */
  void receive();

  /** Input channel getter */
  inline Channel<InpMsg> *input() { return &m_inputChannel; }

  /** Output channel getter */
  inline Channel<OupMsg> *output() { return &m_outputChannel; }

  /** Receive error handler */
  void handleRxError();

  /** Lost connection event setter */
  inline void setLostConnectionEvent(EventSPtr &ev) {
    m_lostConnectionEvent = std::move(ev);
  }


private:
  union InputPackage {
    uint8_t data[bufSize + 2]; // 2 extra bytes for stx and checksum
    struct {
      uint8_t stx;
      InpMsg msg;
    } __attribute__((packed, aligned(1)));
  };

  union OutputPackage {
    uint8_t data[bufSize + 2]; // 2 extra bytes for stx and checksum
    struct {
      uint8_t stx;
      OupMsg msg;
    } __attribute__((packed, aligned(1)));
  };

  Channel<InpMsg> m_inputChannel;
  Channel<OupMsg> m_outputChannel;

  void setLostConnection(bool b);

  static constexpr uint8_t MaxErrorCount = 10;
  uint8_t calculateChecksum(uint8_t *data, size_t length);

  IODevice *m_ioDevice;
  uint8_t m_errorCount;

  bool m_lostConnection;
  EventSPtr m_lostConnectionEvent;
};

#include "intercom.inl"
} // namespace uintercom
