template <typename InpMsg, typename OupMsg, size_t bufSize>
Intercom<InpMsg, OupMsg, bufSize>::Intercom(IODevice *device) :
	m_ioDevice(device)
{
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::init()
{
	m_errorCount = 0;
	m_ioDevice->open();
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::deinit()
{
	m_ioDevice->close();
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::transmit()
{
    // copy current values from the input channel
    OutputPackage m_output = { .msg = m_outputChannel.acquire() };
    m_outputChannel.release();
    constexpr size_t length = bufSize + 2; // package + stx and checksum bytes
    // the STX code is equal to the number of bytes in the package
	m_output.stx = length;
    // the last byte in the package is the checksum byte
	m_output.data[length - 1] = calculateChecksum(m_output.data, length - 1);
	m_ioDevice->write(m_output.data, length);
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::receive()
{
    constexpr size_t length = bufSize + 2; // package + stx and checksum bytes
	InputPackage input;

    // if number of read bytes is smaller than the size
    // of the buffer, then consider it an Rx error
	if (m_ioDevice->read(input.data, length) != length){
        handleRxError();
    }
    else {
        // calculate checksum and compare it with the received one
        uint8_t checksum = calculateChecksum(input.data, length - 1);

        if (checksum != input.data[length - 1]) {
            handleRxError();
        }
        else {
            // reset the number of Rx error in a row
            m_errorCount = 0;
            setLostConnection(false);
            m_inputChannel.acquire() = input.msg;
            m_inputChannel.release();
        }
    }
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::handleRxError()
{
    if(++m_errorCount > MaxErrorCount) {
        setLostConnection(true);
    }
}


template <typename InpMsg, typename OupMsg, size_t bufSize>
uint8_t Intercom<InpMsg, OupMsg, bufSize>::calculateChecksum(uint8_t *data, size_t size)
{
    // simple checksum as the inververse of the sum of all bytes in the package
	uint8_t sum = 0;
	for (size_t i = 0; i < size; i++) {
		sum += data[i];
	}
	return ~sum;
}

template <typename InpMsg, typename OupMsg, size_t bufSize>
void Intercom<InpMsg, OupMsg, bufSize>::setLostConnection(bool b)
{
	if (m_lostConnection == b) return;

    m_lostConnection = b;

    if (m_lostConnectionEvent) {
        m_lostConnectionEvent->post(b);
    }
}
